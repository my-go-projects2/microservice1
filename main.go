package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// func handler(w http.ResponseWriter, r *http.Request) {
//     fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
// }

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("this is log")
		d, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Ooops", http.StatusBadRequest)
		// w.WriteHeader(http.StatusBadRequest)
		// w.Write([]byte("OOPS"))
		return
	
	}
		log.Printf("Data %s\n", d)

		fmt.Fprintf(w, "Hello %s", d)
	})



	http.HandleFunc("/goodbye", func(w http.ResponseWriter, r *http.Request) {

		log.Println("gOODBY")
	} )

	http.ListenAndServe(":9090", nil)
}